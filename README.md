# From Zero

This is a repository with all code created for my Ruby Dojos.

*Please send me an [email](mailto:danyfiorot@gmail.com) if you need any help or have any suggestion.*

---

## Clone the repository

Clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

- SSH:
git clone git@bitbucket.org:d4n1vf/from_0.git

- HTTPS:
git clone https://d4n1vf@bitbucket.org/d4n1vf/from_0.git