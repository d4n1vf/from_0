# Google Maps API

For API testing practice, you can try automating Google Maps APIs Web Services (RESTful services). but in order to access google API services, it is mandatory that you should get the API KEY first.

Refer this link for getting API KEY: https://developers.google.com/maps/documentation/javascript/get-api-key

Google API Services :
https://developers.google.com/maps/web-services/overview

Google Maps Directions API
Google Maps Distance Matrix API
Google Maps Elevation API
Google Maps Geocoding API
Google Maps Geolocation API
Google Maps Roads API
Google Maps Time Zone API
Google Places API Web Service

*Please send me an [email](mailto:danyfiorot@gmail.com) if you need any help or have any suggestion.*

---

## Features

- ????
- ????