=begin
Ao receber uma palavra ou frase, retorne quantos caracteres foram informados, ignorando os espaços em branco.

* Usar o comando STDIN.gets para obter o valor informado no terminal.
* Usar o método length para obter o número de caracteres de uma string.
* Usar o método delete para excluir os espaços em branco.
* Usar o método strip para excluir a quebra de linha.
=end

puts 'Informe a palavra/frase: '
words = STDIN.gets

puts 'Resposta: '
puts words.strip.delete!(' ').length
# puts words.gsub(/\s+/, "").length