=begin
Ao receber uma palavra ou frase, retorne o eco 3 vezes do que foi recebido.

* Usar o comando STDIN.gets para obter o valor informado no terminal.
=end

puts 'Informe a palavra/frase: '
words = STDIN.gets

puts 'Resposta: '
3.times do
    puts words
end