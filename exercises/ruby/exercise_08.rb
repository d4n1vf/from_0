=begin
Todas as páginas de um Web Site possuem um Header (que tem um Título e um Logo), um texto e uma imagem.

Crie uma classe que represente uma dessas páginas considerando que:
Cada página possui título, texto e imagem diferente das demais páginas, mas o Logo sempre é o mesmo;
Todas as páginas usam um mesmo template.
=end

class Header
  attr_accessor :field_logo, :field_title, :text_title

  def initialize(p_title)
    self.field_logo = 'id-logo'
    self.field_title = 'id-title'
    self.text_title = p_title
  end
end

class Header < SitePrism::Page
  element field_logo, 'id-logo'
  element field_title, 'id-title'

  def initialize(p_title)
    self.field_logo.set 'logo.png'
    self.text_title.set p_title
  end
end

class MainPage
  # elementos da minha página
  attr_accessor :header, :field_text, :text, :field_image, :image

  def initialize(p_title, p_text, p_image)
    self.header = Header.new(p_title)
    self.field_text = 'id-text'
    self.field_image = 'id-image'
    self.text = p_text
    self.image = p_image
  end
end

class MyPage < MainPage
  def load
    puts header.text_title, text, image
  end
end

page = MyPage.new('Minha Página', 'Meu Texto Aqui!', '/imagem_minha')
page.load()
