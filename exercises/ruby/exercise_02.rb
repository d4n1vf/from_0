=begin
Calcule o seguinte:
Quantas horas tem um ano?
Quantos minutos tem uma década?
Qual a sua idade em segundos?
=end

puts "Quantas horas tem um ano? #{365*24} horas"
puts "Quantos minutos tem uma década? #{10*365*24*60} minutos"
puts "Qual a sua idade em segundos? #{32*365*24*60*60} segundos"