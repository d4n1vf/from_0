=begin
Ao receber uma hora do dia (no formato HH), responda com "Bom dia", "Boa tarde" ou "Boa noite".

* Usar o comando STDIN.gets para obter o valor informado no terminal.

Exemplo: Recebido a hora 13
Resposta: Boa tarde
=end

puts 'Informe a hora: '
hour = STDIN.gets.to_i

puts 'Resposta: '
case
when hour < 12
  puts 'Bom dia'
# when hour > 12 && hour < 17 # OR
# when (12...17) == hour # OR
when hour < 17
  puts 'Boa tarde'
else
  puts 'Boa noite'
end
