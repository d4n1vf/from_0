=begin
Criar uma classe para acessar a API de To-Dos (https://jsonplaceholder.typicode.com/todos) com os HTTP verbs GET, POST, PUT e DELETE.
=end

require 'httparty'

class TodoService
  include HTTParty
  base_uri 'https://jsonplaceholder.typicode.com'
  format :json

  def initialize
    # first steps
  end

  def get_todos
    return self.class.get('/todos')
  end

  def get_todo_by_id(todo_id)
    return self.class.get("/todos/#{todo_id}")
  end

  def post_todo(todo_id)
    options = {
      body: {
        userId: 1,
        id: todo_id,
        title: "teste post to-do",
        completed: false
      }
    }
    return self.class.post('/users', options)
  end

  def delete_todo_by_id(todo_id)
    return self.class.delete("/todos/#{todo_id}")
  end
end

todo_api = TodoService.new
puts 'GET To-dos:'
response = todo_api.get_todos
puts "Status Code: #{response.code}"
puts "Response: #{response}"
puts 'GET To-do 1:'
response = todo_api.get_todo_by_id(1)
puts "Status Code: #{response.code}"
puts "Response: #{response}"
puts 'POST New To-do:'
response = todo_api.post_todo(999999)
puts "Status Code: #{response.code}"
puts "Response: #{response}"
puts 'DELETE To-do:'
response = todo_api.delete_todo_by_id(1)
puts "Status Code: #{response.code}"
puts "Response: #{response}"
