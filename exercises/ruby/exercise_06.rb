=begin
Pedindo o peso e a altura, calcule o IMC e informe conforme a seguir:

Se IMC < 20: “Abaixo do Peso Ideal”
Se IMC >= 20 e < 25: “Peso Normal”
Se IMC >= 25 e < 30: “Excesso de Peso”
Se IMC >= 30 e <= 35: “Obesidade”
Se IMC > 35: “Super Obesidade”

* Usar o comando STDIN.gets para obter o valor informado no terminal.
=end

puts 'Informe o seu peso (em quilogramas, exemplo 79.5)'
peso = STDIN.gets.to_f

puts 'Informe a sua altura (em metros, exemplo 1.75)'
altura = STDIN.gets.to_f

# Cálculo do IMC
# início da função
def calcula_imc(p, a)
    return ((p) / (a ** 2)).round(2)
end
# fim da função

# Verifica IMC Completo
# RN1 - Se IMC < 20: Abaixo do Peso Ideal
# RN2 - Se IMC >= 20 e < 25: Peso Normal
# RN3 - Se IMC >= 25 e < 30: Excesso de Peso
# RN4 - Se IMC >= 30 e <= 35: Obesidade
# RN5 - Se IMC > 35: Super Obesidade
def verifica_imc(i)
    case i
        when 0..20
            puts "IMC: #{i}. Abaixo do Peso Ideal"
        when 20..25
            puts "IMC: #{i}. Peso Normal"
        when 25..30
            puts "IMC: #{i}. Excesso de Peso"
        when 30..35
            puts "IMC: #{i}. Obesidade"
        else
            puts "IMC: #{i}. Super Obesidade"
    end
end

imc = calcula_imc(peso, altura)
verifica_imc(imc)