=begin
Ao receber dois números, retorne todos os números inteiros pares entre eles.

* Usar o comando STDIN.gets para obter o valor informado no terminal.

Exemplo: Recebido os número 5 e 8
=end

puts 'Informe o primeiro número: '
num1 = STDIN.gets.to_i

puts 'Informe o segundo número: '
num2 = STDIN.gets.to_i

puts 'Resposta: '
for i in num1..num2
    puts i if i.even? # (i % 2) == 0
end