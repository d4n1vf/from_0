# Class for all JSON manipulations
class JSONManipulation
    # Method to parse JSON file
    def parse_json(file_name, file_path)
      full_path = File.join(file_path, file_name)
      read_json = File.read(full_path)
      JSON.parse(read_json)
    rescue StandardError => exception
      raise exception.message
    end
  
    # Method to compare JSON schema and return true if has difference between two JSONs
    def compare_schema_json(base_schema, compare_schema)
      compare_keys = base_schema.keys - compare_schema.keys
      puts compare_keys
      compare_values = base_schema.values - compare_schema.values
      puts compare_values
      return [compare_keys, compare_values], true if (compare_keys.empty? == false && compare_values.empty? == false)
      false
    end
  end