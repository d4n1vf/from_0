require 'httparty'

class EchoService
  include HTTParty
#   base_uri 'http://echo.jsontest.com/'
  format :json

  def initialize
    # first steps
  end

  def get_echo(dict)
    parameters = ''
    puts 'dict = ' + dict.to_s
    for key,value in dict do
        puts 'key' + key.to_s
        puts 'value' + value.to_s
        parameters += "#{key}/#{value}/"
    end
    puts 'parameters = ' + parameters
    return self.class.get('http://echo.jsontest.com/'+parameters)
  end
end

# echo_api = EchoService.new
# puts 'GET Echo:'
# response = echo_api.get_echo({teste: 'som', teste2: 'som2'})
# puts "Status Code: #{response.code}"
# puts "Response: #{response}"