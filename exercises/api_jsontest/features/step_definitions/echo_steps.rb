Given("I have") do |table|
  @voz = table.rows_hash
  @voz.delete('key')
  puts @voz
end

When("I call Echo") do
  echo_api = EchoService.new
  @response = echo_api.get_echo(@voz)
end

Then("I got status code as {int}") do |status_code|
  expect(@response.code).to be status_code
end

Then("Response is {string}") do |expected_return|
  j = JSONManipulation.new
  expect_json = j.parse_json(expected_return, "#{Dir.pwd}/data/")
  puts expect_json
  puts @response
  compare = j.compare_schema_json(expect_json, @response)
  # expect(compare).to be false
  expect(@response.parsed_response).to eql expect_json
end