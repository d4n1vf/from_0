# Ice Cream Truck

- Have a stock including Quantity and Unit Price for each Flavour
- Every 3 ice creams the fourth is free
- Only can sell an ice cream if receives enough money that pays it

*Please send me an [email](mailto:danyfiorot@gmail.com) if you need any help or have any suggestion.*

---

## Features

- Stock:
    - Add on stock a flavour including quantity and unit price.
    - If the flavour already exists, add the informed quantity to the exists one and update the unit price.
    - If the quantity is less than 5, don't let update it and send a message.
    - If the new price is less than $1, don't let update it and send a message.
    - If the new price is 3 times cheaper or more, don't let update it and send a message.
    - If the new price is 2 times more expensive or more, don't let update it and send a message.
- Selling:
    - Took from stock a number of ice creams of a flavour, given some money and getting change and a number of ice creams back.
    - Every 3 ice creams the fourth is free.
    - If not receives enough money, don't sell and send a message.
    - If receives more money, return change back.
    - If receives enough money, return only the ice cream.