require_relative '../../app/stock'

Given("a new flavour {string} with {int} units costing ${int} each") do |flavour, qty, unit_price|
    @flavour = {name: flavour, qty: qty, unit_price: unit_price}
end

When("I Add it to the stock") do
    @stock = Stock.new
    @stock.add(@flavour)
end

Then("My stock will have a new flavour") do
    # expect(actual).to be >  expected
    # expect(actual).to be >= expected
    # expect(actual).to be <= expected
    # expect(actual).to be <  expected
    # expect(actual).to be_between(minimum, maximum).inclusive
    # expect(actual).to be_between(minimum, maximum).exclusive
    # expect(actual).to match(/expression/)
    # expect(actual).to be_within(delta).of(expected)
    # expect(actual).to start_with expected
    # expect(actual).to end_with expected
    expect(@stock.list_of_flavours.size).to be > 0
    expect(@stock.list_of_flavours[0]['name']).to be @flavour['name']
    expect(@stock.list_of_flavours[0]['qty']).to be @flavour['qty']
    expect(@stock.list_of_flavours[0]['unit_price']).to be @flavour['unit_price']
end