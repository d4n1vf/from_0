Feature: Stock
    Add on stock a flavour including quantity and unit price.
    If the flavour already exists, add the informed quantity to the exists one and update the unit price.
    If the quantity is less than 5, don't let update it and send a message.
    If the new price is less than $1, don't let update it and send a message.
    If the new price is 3 times cheaper or more, don't let update it and send a message.
    If the new price is 2 times more expensive or more, don't let update it and send a message.

    # Background: Current Stock
    #     Given I have a Stock with
    #         | flavour   | quantity | unit price |
    #         | chocolate | 10       | 1          |
    #         | vanilla   | 15       | 1          |
    #         | pineaple  | 5        | 1          |
    #         | apple     | 5        | 1          |

    Scenario: Adding new flavour
        Given a new flavour "banana" with 10 units costing $2 each
        When I Add it to the stock
        Then My stock will have a new flavour

