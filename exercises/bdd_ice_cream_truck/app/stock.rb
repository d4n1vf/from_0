class Stock
    attr_accessor :list_of_flavours

    def initialize
        self.list_of_flavours = []
    end 

    def add(flavour)
        self.list_of_flavours << flavour
    end
    
end