list = %W{Zero One Two Three Four Five Six}

# FOR EACH
# (expression).each do |variable[, variable...]|
#     code
# end
list.each do |item|
  if item.include?('e')
    puts item
  end
end
puts ''
["first", "middle", "last"].each { |i| puts i.capitalize }
puts ''
["first", "middle", "last"].reverse_each { |i| puts i.upcase }
puts ''
["a", "b", "c"].each_with_index do |letter, index| 
  puts "#{letter.upcase}: #{index}"
end

puts ''
puts ''

# FOR
# for variable [, variable ...] in expression [do]
#     code
# end
for i in 0..(list.size - 1)
  item = list[i]
  unless item.include?('e')
    puts item
  end
end
puts ''
joe = {:name => "Joe", :age => 30, :job => "plumber"}
for key, val in joe
  puts "#{key} is #{val}"
end

puts ''
puts ''

# WHILE DO
# while conditional [do]
#     code
# end
idx = 0
max = list.size
while idx < max
  item = list[idx]
  if item.include?('e')
    puts item
  end
  idx += 1
end

puts ''
puts ''

# DO WHILE
# begin
#     code
# end while conditional
arr = ["John", "George", "Paul", "Ringo"]
puts 'Beatles: ' + arr.to_s
i = -1
puts arr[i += 1] while arr[i]

puts ''
puts ''

# UNTIL DO
# until conditional [do]
#     code
# end
days_left = 7
until days_left == 0
  puts "there are still #{days_left} in the week"
  days_left -= 1
end

puts ''
puts ''

# DO UNTIL
# begin
#     code
# end until conditional
days_left = 8
puts "there are still #{days_left -= 1} in the week" until days_left == 1

puts ''
puts ''

# Partial Iteration
[1,2,3,4,5][0..2].each { |i| puts i }
# or
arr = [1,2,3,4,5]
(0..2).each { |i| puts arr[i] }

puts ''
puts ''

# Map
list = [1,2,3]
puts (list.each { |i| i + 1 }).to_s
puts (list.map { |i| i + 1 }).to_s
puts ([1,2,3].map(&:to_f)).to_s
puts ([1,2,3].map { |i| i.to_f }).to_s

puts ''
puts ''

# Classical Iteration (https://stackoverflow.com/questions/10396475/ruby-for-loop-a-trap)
results = []
(1..3).each { |i| results << lambda { i } }
puts results.map(&:call)
puts ''
results = []
for i in 1..3
   results << lambda { i }
end
puts results.map(&:call).to_s