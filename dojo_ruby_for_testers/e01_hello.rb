# Single line comment

=begin
Multi
Line
Comment
=end

# print Hello Word on Console

# single-quotes
puts 'Hello World'
p 'Hello World'
print 'Hello World'

puts ''
puts ''

# double-quotes
puts "Hello World"
p "Hello World"
print "Hello World"

puts ''
