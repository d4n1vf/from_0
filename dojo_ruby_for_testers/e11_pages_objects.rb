class LoginPage

  # elementos da minha página
  attr_accessor :campo_usuario, :campo_senha, :botao_entrar

  def initialize
    self.campo_usuario = 'id-text-usuario'
    self.campo_senha = 'id-text-senha'
    self.botao_entrar = 'class-btn-entrar'
  end

  def entrar(email, senha)
    puts "preenchendo o valor #{email} no campo usuário do elemento #{campo_usuario}"
    puts "preenchendo o valor #{senha} no campo senha do elemento #{campo_senha}"
    puts "clicando no elemento #{botao_entrar} "
  end
end

login = LoginPage.new
login.entrar('eu@gmail.com', '123456')
