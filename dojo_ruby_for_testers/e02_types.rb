# Identifying Variable Type (y.kind_of? Integer)
var = '1'
puts var.class
var = 1
puts var.class
var = 1.1
puts var.class
var = true
puts var.class
var = []
puts var.class
var = {}
puts var.class

puts ''
puts ''

# Converting from String
var = '54321'
puts var.class
var = '54321'.to_i # to integer
puts var.class
var = '54321'.to_f # to float
puts var.class

puts ''
puts ''

# Converting from Integer/Fixnum
var = 54321
puts var.class
var = 54321.to_s # to string
puts var.class
var = 54321.to_s(2) # to binary string
puts var.class
var = 54321.to_s(8) # to octal string
puts var.class
var = 54321.to_s(16) # to hexadecimal string
puts var.class
var = 54321.to_f # to float
puts var.class

puts ''
puts ''

# Converting from Float
var = 5432.1
puts var.class
var = 5432.1.to_s # to string
puts var.class
var = 5432.1.to_i # to integer
puts var.class

puts ''
puts ''

# Converting from Boolean
var = true
puts var.class
var = var.to_s # to string
puts var.class

puts ''
puts ''

# Converting from Array
var = []
puts var.class
var = var.to_s # to string
puts var.class

puts ''
puts ''

# Converting from Hash
var = {}
puts var.class
var = var.to_s # to string
puts var.class
