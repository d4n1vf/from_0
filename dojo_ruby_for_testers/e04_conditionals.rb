condition_1 = false
condition_2 = true

# If Statement
if condition_1
  puts 'condition 1 is true'
elsif condition_2
  puts 'condition 2 is true'
else
  puts 'no condition is true'
end

# If as a Modifier
puts 'condition 2 is true' if condition_2

puts ''
puts ''

# Unless Statement
unless condition_1
  puts 'condition 1 is false'
end

# Unless as a Modifier
puts 'condition 1 is false' if condition_1

puts ''
puts ''

# Case / When Statement
case
when condition_1
  puts 'condition 1 is true'
when condition_2
  puts 'condition 2 is true'
else
  puts 'no condition is true'
end

# Case as a Modifier
message = case
          when condition_1
            'condition 1 is true'
          when condition_2
            'condition 2 is true'
          else
            'no condition is true'
          end
puts message
