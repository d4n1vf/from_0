# Without parameters
def method_01
  puts 'method 01'
end
method_01

puts ''
puts ''

# With parameter
def method_02(var1, var2)
  puts "method 02 #{var1} e #{var2}"
end
method_02 1, 2
method_02(1, 2)

puts ''
puts ''

# With default values for parameters
def method_03(var1 = 'value1', var2 = 'value2')
  puts "method 03 #{var1} e #{var2}"
end
method_03
method_03 1, 2
method_03(1, 2)

puts ''
puts ''

# Returns last line
def method_04(var1 = 'value1', var2 = 'value2')
  a = var1
  b = var2
end
puts method_04

puts ''
puts ''

# Returns one value
def method_05(var1 = 'value1', var2 = 'value2')
  a = var1
  b = var2
  return a
end
puts method_05

puts ''
puts ''

# Returns more than one value
def method_06(var1 = 'value1', var2 = 'value2')
  a = var1
  b = var2
  return a, b
end
puts method_06

puts ''
puts ''

# Variable Number of Parameters
def method_07(*test)
  puts "The number of parameters is #{test.length}"
  for i in 0...test.length
    puts "The parameters are #{test[i]}"
  end
end
method_07("Zara", "6", "F")
method_07("Mac", "36", "M", "MCA")
