require 'httparty'

class UserService
  include HTTParty
  base_uri 'https://jsonplaceholder.typicode.com'
  format :json

  def initialize
    # first steps
  end

  def get_users
    return self.class.get('/users')
  end

  def get_user_by_id(user_id)
    return self.class.get("/users/#{user_id}")
  end

  def post_user(user_id)
    options = {
      body: {
        id: user_id,
        name: 'John Doe',
        username: 'johndoe',
        email: 'johndoe@johndoe.co'
      }
    }
    return self.class.post('/users', options)
  end
end

user_api = UserService.new
puts 'GET Users:'
response = user_api.get_users
puts "Status Code: #{response.code}"
puts "Response: #{response}"
puts 'GET User 1:'
response = user_api.get_user_by_id(1)
puts "Status Code: #{response.code}"
puts "Response: #{response}"
puts 'POST New User:'
response = user_api.post_user(999999)
puts "Status Code: #{response.code}"
puts "Response: #{response}"
