class Animal
  attr_accessor :nome, :raca, :cor

  # construtor
  def initialize(n, r, c)
    self.nome = n
    self.raca = r
    self.cor = c
  end

  def olhar
    puts "#{nome} é #{cor} da raça #{raca}"
  end

  def comer(racao)
    puts "#{nome} comendo #{racao}"
  end
end

class Dog < Animal
  def fala
    puts "#{nome} diz: au au"
  end
end

class Cat < Animal
  def fala
    puts "#{nome} diz: miau"
  end
end

dog1 = Dog.new('Spike', 'Buldog', 'preto')
dog1.olhar
dog1.fala
dog1.comer('Frolic')

dog2 = Dog.new('Beethoven', 'São Bernardo', 'marrom e branco')
dog2.olhar
dog2.fala
dog2.comer('Purina')

dog3 = Dog.new('Snoop', 'Beagle', 'branco')
dog3.olhar
dog3.fala
dog3.comer('Champs')

cat1 = Cat.new('Tomm', 'Siamês', 'cinza')
cat1.olhar
cat1.fala
cat1.comer('Wiskas')
