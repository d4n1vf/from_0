class MainPage < SitePrism::Page

  set_url 'http://www.google.com'
  element :input_search, 'input[name=q]'
  element :button_search, 'input[name=btnK]'
  element :button_lucky, 'input[name=btnI]'
  elements :results, 'div.g'

  def search(text)
    input_search.set text
    button_search.click
  end

  def i_am_feeling_lucky(text)
    input_search.set text
    button_lucky.click
  end
end