string_1 = 'string 1'
string_2 = 'string 2'
bool_1 = true
bool_2 = false
int_1 = 4
int_2 = 2
float_1 = 10.5
float_2 = 20.5

# Concatenate Strings
puts string_1 + string_2 # Concatenate

puts ''
puts ''

# Interpolation of Strings
puts "#{string_1} #{int_1} #{bool_1} #{float_1}"

puts ''
puts ''

# Parallel Assignment
a = 10
b = 20
c = 30
puts "#{a}, #{b}, #{c}"
a, b, c = 10, 20, 30
puts "#{a}, #{b}, #{c}"

puts ''
puts ''

# Arithmetic Operators
puts int_1 + int_2 # Addition
puts int_1 - int_2 # Subtraction
puts int_1 * int_2 # Multiplication
puts int_1 / int_2 # Division
puts int_1 % int_2 # Modulus
puts int_1**int_2 # Exponent

puts ''
puts ''

# Comparison Operators
puts 'Equal'
puts int_1 == int_1 # Equal
puts int_1 == int_2 # Equal
puts int_1.eql? int_1 # Equal
puts int_1.eql? int_2 # Equal
puts 'Different'
puts int_1 != int_2 # Different
puts int_1 != int_1 # Different
puts 'Less than'
puts int_1 < int_2 # Less than
puts int_1 < int_1 # Less than
puts 'Greater than'
puts int_2 > int_1 # Greater than
puts int_1 > int_2 # Greater than
puts 'Less than or equal to'
puts int_1 <= int_2 # Less than or equal to
puts int_1 <= int_1 # Less than or equal to
puts int_2 <= int_1 # Less than or equal to
puts 'Greater than or equal to'
puts int_2 >= int_1 # Greater than or equal to
puts int_2 >= int_2 # Greater than or equal to
puts int_1 >= int_2 # Greater than or equal to
puts 'Combined comparison operator'
puts int_1 <=> int_1 # Combined comparison operator
puts int_1 <=> int_2 # Combined comparison operator
puts int_2 <=> int_1 # Combined comparison operator
puts int_1 <=> int_1 # Combined comparison operator

# Equal on case statement
puts 'Equal on case statement'
puts (1...10) === 5 # True
puts (1...4) === 5 # False

# Equal for objects
puts 'Equal for objects'
a = { "x" => 1 }
b = { "x" => 1 }
c = a
puts a == b # True
puts a == a # True
puts a == c # True
puts a.equal? b # False
puts a.equal? a # True
puts a.equal? c # True

puts ''
puts ''

# Logical Operators
puts 'Logical Operators'
puts bool_1 and bool_1
puts bool_1 and bool_2
puts bool_2 and bool_2
puts bool_1 && bool_1
puts bool_1 && bool_2
puts bool_2 && bool_2
puts bool_1 or bool_1
puts bool_1 or bool_2
puts bool_2 or bool_2
puts bool_1 || bool_1
puts bool_1 || bool_2
puts bool_2 || bool_2
puts not(bool_1)
puts not(bool_2)
puts !(bool_1)
puts !(bool_2)
