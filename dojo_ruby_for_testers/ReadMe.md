# **Ruby**

## Documentation
https://www.ruby-lang.org/en/documentation/


## Installation
https://github.com/oneclick/rubyinstaller2/releases/download/rubyinstaller-2.4.4-1/rubyinstaller-devkit-2.4.4-1-x64.exe


## RubyGems
https://rubygems.org/


## Basic Commands

### Check Ruby Version
ruby -v

### Run Ruby File
ruby file.rb


## Try Ruby
https://ruby.github.io/TryRuby/


## Exercises
https://www.w3resource.com/ruby-exercises/
http://ruby-for-beginners.rubymonstas.org/exercises.html
https://launchschool.com/books/ruby/read/intro_exercises