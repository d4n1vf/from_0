begin
    arr = [1,2,3]
    arr.freeze
    arr << 4 
rescue Exception => ex
    puts 'Lista congelada.'
    puts ex.message
    puts ex.backtrace
end