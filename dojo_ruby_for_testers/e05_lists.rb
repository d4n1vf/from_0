# Empty Array
list = []
puts list.to_s
list = Array.new
puts list.to_s

puts ''
puts ''

# Empty Array with start size
list = Array.new(3)
puts list.to_s

puts ''
puts ''

# Empty Array with start size and default values
list = Array.new(3, 0)
puts list.to_s

puts ''
puts ''

# Full Array definition
list = ['One', 'Two', 'Three', 'Four']
puts list.to_s
list = Array.new(['One', 'Two', 'Three', 'Four'])
puts list.to_s
four = 'Four'
list = %W{One Two Three #{four}}
puts list.to_s
list = %w{One Two Three #{four}}
puts list.to_s

puts ''
puts ''

# Heterogeneous Array
list = ["bob", 3, 0.931, true]
puts list.to_s

puts ''
puts ''

# Array Indices
list = []
puts list[3].to_s
list[4] = 'Five'
puts list.to_s
list = ['One', 'Two', 'Three', 'Four']
puts list[-1].to_s
puts list[-3].to_s
puts list[-8].to_s

puts ''
puts ''

# Number of items on Array
puts list.size

puts ''
puts ''

# Array Ranges
list = %w{a b c d e f}
puts list.to_s
# ["a", "b"]: give me elements 0 through 1
puts list[0..1].to_s
# ["b", "c", "d"]: starting with index 1, give me 3 elements
puts list[1, 3].to_s 
# ["c", "d", "e"]: give me elements 2 until 5
puts list[2...5].to_s 
# ["a", "b", "c", "d", "e"]: give me elements 0 through -2
puts list[0..-2].to_s 
# ["d"]: starting with element -3, give me 1 elements
puts list[-3, 1].to_s 

puts ''
puts ''

# Adding item on Array
list = %W{One Two Three Four}
list << 'Five'
puts list.to_s
list.push('Six')
puts list.to_s
list.unshift('Zero')
puts list.to_s

puts ''
puts ''

# Removing item from Array
list = %W{Zero One Two Three Four Five Six}
list.pop
puts list.to_s
list.shift
puts list.to_s
list.delete('Zero')
puts list.to_s
list.delete_at(1)
puts list.to_s

puts ''
puts ''

# Combined Arrays
list = ['Zero', 'One', 'Two'] + ['Three', 'Four', 'Five', 'Six']
puts list.to_s
list = ['Zero', 'One', 'Two'].concat(['Three', 'Four', 'Five', 'Six'])
puts list.to_s
list = %W{Zero One Two Three Four Five Six} - ['Three', 'Five']
puts list.to_s

puts ''
puts ''

# Boolean Operations
puts ([1,2,3] & [2,3,4]).to_s
puts ([1,2,3] | [2,3,4]).to_s
arr1 = [1,2,3]
arr2 = [2,3,4]
xor = arr1 + arr2 - (arr1 & arr2)
puts xor.to_s

puts ''
puts ''

# Moving Elements
a = [1,2,3]
puts a.to_s
a = [1,2,3].reverse
puts a.to_s
a = [1,2,3].rotate
puts a.to_s
a = [1,2,3].rotate(-1)
puts a.to_s

puts ''
puts ''

# Safeguarding
arr = [1,2,3]
arr.freeze
# arr << 4  
# RuntimeError: can't modify frozen Array

puts ''
puts ''

# Combining Elements Into a String
words = ["every","good","boy","does","fine"]
puts words.join
puts words.join(" ")

puts ''
puts ''

# Removing Nesting
a = [1,[2,3],[4,["a", nil]]]
puts a.to_s
puts a.flatten.to_s
puts a.flatten(1).to_s

puts ''
puts ''

# Removing Duplicates
a = [4,1,2,1,5,4]
puts a.to_s
puts a.uniq.to_s

puts ''
puts ''

# Slicing
a = [1,2,3,4,5]
puts a.to_s
puts a.first(3).to_s
puts a.last(3).to_s

puts ''
puts ''

# Querying
a = ["a","b","c"]
puts a.to_s
puts ["a","b","c"].include? "d"
puts ["a", "a", "b"].count "a"
puts ["a", "a", "b"].count "b"
puts [1,2,[3,4]].size