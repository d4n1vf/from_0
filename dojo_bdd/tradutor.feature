# language: pt
Funcionalidade: Tradutor de Idiomas

   Eu como usuário
   Sei uma língua
   Quero poder entender outra língua

    Cenário: Texto válido
    Dado que tenho o texto "Olá, Mundo!"
    E o tradutor conhece português e inglês
    Quando peço a tradução
    Então recebo o texto "Hello, World!"

    Cenário: Texto inválido
    Dado que tenho o texto "0l4, Mund0!"
    E o tradutor conhece português e inglês
    Quando peço a tradução
    Então recebo a mensagem "Tradução não encontrada."

    Cenário: Idioma não cadastrado
    Dado que tenho o texto "Privet, mir!"
    E o tradutor conhece português e inglês
    Quando peço a tradução
    Então recebo a mensagem "Tradução não encontrada."