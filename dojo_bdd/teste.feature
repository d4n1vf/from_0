            # language: pt
            Funcionalidade: Consultar Triângulos

            Cenário: Consultar um Triângulo Escaledo
            TCS-123
            Dado que tenho os lados de um triângulo com as seguintes medições
            | Lado A | Lado B | Lado C |
            | 3      | 4      | 5      |
Quando pesquiso informando essas medições
Então o sistema informa que o triângulo é "Escaleno"

Funcionalidade: JIRA-3456

Cenário: TC001
Dado que eu cliquei no menu "Calculadora"
E eu cliquei no menu "Soma"
E eu preencho o campo "Primeiro número"
E eu preencho o campo "Segundo número"
Quando eu clico no botão "+"
Então eu vejo a mensagem "a soma de 10 + 5 é igual a 15"

Funcionalidade: Calculadora
JIRA-3457

Cenário: Soma
Quando eu somar dois números
Então os números são somados


Funcionalidade: Soma
JIRA-3456
Para fazer a soma
Eu devo informar dois números

Cenário: Somando dois números com sucesso
Dado que eu tenho dois números "10" e "5"
Quando eu soma-los
Então eu recebo a mensagem "A soma de 10 e 5 é igual a 15"

Cenário: Primeiro valor não é um número
Dado que eu tenho dois números "A5" e "5"
Quando eu soma-los
Então eu recebo a mensagem "A soma funciona apenas entre números"


Funcionalidade: Pesquisa Google
Eu como estudante Quero poder pesquisar qualquer coisa Para poder aprender

Cenário: Pesquisa simples com resultados
Dado eu quero pesquisar por "maçã"
Quando eu google isso
Então eu obtenho vários sites

Cenário: Pesquisa simples sem resultados
Dado eu quero pesquisar por "G569j34wr"
Quando eu google isso
Então eu obtenho a mensagem "Sua pesquisa - G569j34wr - não encontrou nenhum documento correspondente."


Feature: Price conditions

    Scenario: Status 400 Bad Request - CustomerID Invalid Number
        Given There Is A Json File X
        And The customerID is 46789745646879878
        And The orderItems size is 1
        And The orderItems[0].itemID is 001
        And The orderItems[0].itemQuantity is 2
        When I Send A Post Request
        Then The service status is 400


Feature: Price conditions

    Scenario: Status 400 Bad Request - CustomerID Invalid Number
        Given There Is A Line
            | customerID        | size | itemID | itemQuantity |
            | 46789745646879878 | 1    | 2      | 2            |
        When I Send The Line
        Then I get 400 as status code
