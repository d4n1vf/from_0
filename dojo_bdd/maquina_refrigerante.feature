# language: pt
Funcionalidade: Máquina de Refrigerante

   Eu como usuário
   Compro um refrigerante
   Para matar minha sede

    Cenário: Tenho dinheiro suficiente
    Dado que tenho 5 reais
    E a máquina tem coca-cola
    Quando compro uma coca-cola
    Então consigo beber refrigerante

    Cenário: Não tenho dinheiro suficiente
    Dado que tenho 2 reais
    E a máquina tem pepsi
    Quando compro uma pepsi
    Então recebo a mensagem "Dinheiro insuficiente."

    Cenário: Máquina não tem o refrigerante que eu quero
    Dado que tenho 5 reais
    E a máquina tem pepsi
    Quando tento comprar uma coca-cola
    Então recebo a mensagem "Estoque zerado."