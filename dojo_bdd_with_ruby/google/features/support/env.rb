require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'rspec'
require 'capybara/rspec'
require 'site_prism'
require 'httparty'

Capybara.default_max_wait_time = 5

BROWSER = ENV['BROWSER']

Capybara.register_driver :ie do |app|
  Capybara::Selenium::Driver.new(app, :browser => :ie)
end

Capybara.configure do |config|
  config.app_host = 'http://www.google.com'

  #  :rack_test, :selenium, :selenium_chrome, :selenium_chrome_headless
  if BROWSER.eql?('chrome')
    config.default_driver = :selenium_chrome
  elsif BROWSER.eql?('headless')
    config.default_driver = :selenium_chrome_headless
  elsif BROWSER.eql?('ie')
    config.default_driver = :ie
  elsif BROWSER.eql?('safari')
    config.default_driver = :safari
  else
    config.default_driver = :selenium_chrome_headless
  end
end