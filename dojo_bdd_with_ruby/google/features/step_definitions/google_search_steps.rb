require_relative '../pages/main_page'

Given("I wanna looking for {string}") do |text|
  @main_page = MainPage.new
  @text_to_search = text
end

When("I google it") do
  @main_page.load
  @results = @main_page.search(@text_to_search)
end

Then("I got a lot of results") do
  expect(@main_page.results.size).to be > 0
end

Then("I got the message {string}") do |text|
  expect(@main_page).to have_content text
end